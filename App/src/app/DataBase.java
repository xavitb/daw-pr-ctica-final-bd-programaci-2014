package app;

import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que permet conectar-se a una Base de Dades Mysql.
 * Permet fer Consultes com Inserts, Selects, Updates i Deletes.
 */
public class DataBase{

    /**
     * Variable que conté la conexió a la Base de Dades
     * 
     * @var Con Connection
     */   
    private Connection Con;
        
    /**
     * Variable que conté el nom de la Base de Dades a la que ens volem conectar
     * 
     * @var db String
     */
    private final String db = "tpv";
    
    /**
     * Variable que conté l'usuari amb el que ens conectam a Base de Dades
     * 
     * @var user String
     */
    private final String username = "root";
    
    /**
     * Variable que conté la contrassenya de l'usuari de la Base de Dades a la 
     * que ens volem conectar
     * 
     * @var password String
     */
    private final String password = "";
    
    /**
     * Variable que conté el servidor de la Base de Dades a la que ens volem conectar
     * 
     * @var host String
     */
    private final String host = "jdbc:mysql://localhost/" + db;
    
    /**
     * Mètode que estableix la conexió a la Base de Dades 
     */
    public void connect() {
        try {
            Con = DriverManager.getConnection(host, username, password);
        } catch (SQLException e) {
            System.out.println("Impossible realitzar conexió amb la Base de Dades");
        }
    }
    
    /**
     * Mètode que tanca la conexió a la Base de Dades
     */
    public void disconnect(){
        try {
            Con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Mètode per formatar l'estructura d'una consulta de tipus Select.
     * 
     * @param Table String. Taula on es realitza la consulta
     * @param Rows String. Nom de les columnes a seleccionar
     * @param Join String. Sintaxis del Join a realitzar
     * @param Where String. Condició per filtrar els resultats
     * @param Order String. Ordre amb que es mostren els resultats
     * @param Limit String. Numero màxim de resultats que es mostraran
     * 
     * @return Query ResultSet
     */
    public ResultSet select(String Table, String Rows, String Join, String Where, String Order, String Limit){
        
        ResultSet rs = null;
        String Query = "SELECT " + Rows + " FROM " + Table;
        
        if(Join != null)
            Query += " JOIN " + Join;
        
        if(Where != null)
            Query += " WHERE " + Where;
        
        if(Order != null)
            Query += " ORDER BY " + Order;
        
        if(Limit != null){
            Query += " LIMIT " + Limit;
        }
        
        try {
            Statement stmt = Con.createStatement();
            rs = stmt.executeQuery(Query);
            
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return rs;
    }
    
    /**
     * Mètode per formatar l'estructura d'una consulta de tipus Insert i
     * realitzar la consulta.
     * 
     * @param Taula String
     * @param Columnes String[]
     * @param Valors String[]
     * 
     */
    public void insert(String Taula, String[] Columnes, String[] Valors){
        
        String Cols = arrayToString(Columnes, " ");
        String Vals = arrayToString(Valors, "'");
        
        String Query = "INSERT INTO " + Taula + " (" + Cols  + ") VALUES (" + Vals + ")";
        
        try {
            Statement stmt = Con.createStatement();
            stmt.executeUpdate(Query);
            
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Mètode per formatar l'estructura d'una consulta de tipus Delete i
     * realitzar la consulta.
     * 
     * @param taula
     * @param condicio 
     */
    public void delete(String taula, String condicio){
        
        String query = "DELETE FROM " + taula + " WHERE " + condicio;

        try {
            Statement stmt = Con.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Mètode que formata una array a una string separant cada element de la 
     * array per comes. Conseguint aixi un format adequat per fer les consultes
     * a la Base de Dades
     * 
     * @param array String[]
     * @param separator
     * 
     * @return String;
     */
    private String arrayToString(String[] array, String separator){
        
        StringBuilder stringComma = new StringBuilder();

        for (String n : array) {
            stringComma.append(separator).append(n.replaceAll(separator, "\\\\'")).append(separator+",");
        }

        stringComma.deleteCharAt(stringComma.length() - 1);

        return stringComma.toString();
    }
}