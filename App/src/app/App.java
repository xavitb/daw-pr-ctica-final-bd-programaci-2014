package app;

import java.util.Scanner;

/**
 * Aplicació per emmagatzemar i analitzar les vendes d'una empresa.
 * 
 * @autor Aitor Delgado Vives, Joan Ingasi Parera Calvo, Xavier Tristancho Bordoy
 */
public class App {

    /**
     * Funció que fa la primera cridad a l'aplicació
     *
     * @param args
     */
    public static void main(String[] args) {

        /**
         * Variable que conté el valor adient corresponent a l'opció del menú
         * que volem seleccionar.
         * 
         * @var exit int
         */
        int exit = 0;
        
        /**
         * Variable que conté el títol de l'aplicació
         * 
         * @var Title String
         */
        String Title = "Analitzador de ventes";

        /**
         * Array que conté les diferents opcions del menú
         * l'última opció sempre serà la de sortida
         * @var options String[]
         */
        String[] options = {"Introduir una venda",
                            "Eliminar venda",
                            "Llistar vendes de un mes",
                            "Llistar productes per ordre de vendes",
                            "Sortir"};
        
        /**
         * Bucle que mostra el menu al acabar de realitzar una acció mentres no 
         * es passi el paràmetere de sortida
         */
        do {
            GUI Interficie = new GUI(Title, options);
            
            /**
             * Llegim l'entrada per teclat
             */
            Scanner in = new Scanner(System.in);
            
            /**
             * Comprovam que s'introdueixi una opció vàlida
             */
            while (!in.hasNextInt()) in.next();
                
            /**
            * Convertim la variable in a tipus int
            */
            int i = in.nextInt();
            
            if((i > options.length)||(i<1)){
                System.out.println("El nombre que heu introduit ("+i+") es erroni. 1-5.\n");
            }
            
            /**
             * Depenent de l'opció introduida cridam el mètode corresponent de la Classe GUI
             */
            switch(i){
                
                case 1:
                    Interficie.introduirVenda();
                break;
                    
                case 2:
                    Interficie.eliminarVenda();
                break;
                    
                case 3:
                    Interficie.llistarVendesMes();
                break;
                    
                case 4:
                    Interficie.llistarProductesMes();
                break;
            }
            
            if(i == options.length){
                            
                System.out.println("Sortir.\n");
                exit=5;
            }
            
            
        }while (exit != options.length);
    }
}