package app;

/**
 * Classe per definir un producte i els seus atributs
 */
public class Producte {
    
    /**
     * Identificador del producte
     * 
     * @var producteID String
     */
    private String producteID;
    
    /**
     * Quantitat total del producte
     * 
     * @var quantitat String
     */
    private String quantitat;
    
    /**
     * Array que conté les columnes de la taula a la Base de Dades
     */
    public final static String[] columnesTaula = {"producte_idproducte", "total"};
    
    /**
     * Constructor de la Classe
     * Accepta una array de dades i les introdueix dins l'atribut corresponent
     * 
     * @param dades String[]
     */
    public Producte(String[] dades){
        
        setProducteID(dades[0]);
        setQuantitat(dades[1]);
    }

    /**
     * Estableix el valor de l'atribut producteID
     * 
     * @param producteID String
     */
    private void setProducteID(String producteID) {
        this.producteID = producteID;
    }

    /**
     * Estableix el valor de l'atribut quantitat
     *
     * @param quantitat String
     */
    private void setQuantitat(String quantitat) {
        this.quantitat = quantitat;
    }
    
    /**
     * Formata l'impressió els atributs de l'objecte
     *
     * @return String
     */
    public String toString(){
        
        String unitats = "unitat";
        
        if(Integer.parseInt(quantitat) > 1)
            unitats += "s";
        
        return "producte " + producteID + ": " + quantitat + " " + unitats;
    }
}
