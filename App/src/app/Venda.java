package app;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que representa una Venda amb els atributs i mètodes corresponents
 * Permet insertar i eliminar una venda dins la Base de Dades
 */
public class Venda {
    
    /**
     * Taula que s'ultilitzarà per fer les consultes a la Base de Dades
     * 
     * @var Taula String
     */
    private final String Taula = "Venda";
    
    /**
     * Array que conté les columnes de la taula Venda
     *
     * @var columnesTaula String[]
     */
    public static final String[] columnesTaula = {"datavenda", "quantitat", "producte_idproducte"};
    
    /**
     * Data en que es realitzà la venda
     * 
     * @var data String
     */
    private String data;
    
    /**
     * Identidficador del producte que es va vendre
     * 
     * @var producteID String
     */
    private String producteID;
    
    /**
     * Quantitat del producte que es va vendre
     * 
     * @var quantitat String;
     */
    private String quantitat;
    
    /**
     * Array que conté les dades que es requeriràn a l'hora de introduir una 
     * venda i el rang númeric vàlid
     * 
     * @var Dades String[][]
     */
    public static final String[][] Dades = {{"Dia", "1", "31"} , 
                                            {"Mes", "1", "12"} , 
                                            {"Any", "1600", "3000"} ,
                                            {"ID Producte", "1", "0"},
                                            {"Quantitat", "0", "0"}
    };
    
    /**
     * Instanciació de la Base de Dades
     *
     * @var DataBase DataBase
     */
    private static final DataBase DataBase = new DataBase();
      
    /**
     * Constructor de la Classe
     * Accepta una array de dades i les introdueix dins l'atribut corresponent
     * 
     * @param dades String[]
     */
    public Venda(String[] dades){
        
        setData(dades[0]);
        setProducteID(dades[1]);
        setQuantiat(dades[2]);
    }
    
    /**
     * Estableix el valor de l'atribut data
     * 
     * @param data String
     */
    private void setData(String data) {
        this.data = data;
    }

    /**
     * Estableix el valor de l'atribut producteID
     *
     * @param producteID String
     */    
    private void setProducteID(String producteID) {
        this.producteID = producteID;
    }

    /**
     * Estableix el valor de l'atribut quantitat
     * 
     * @param quantitat String
     */
    private void setQuantiat(String quantitat) {
        this.quantitat = quantitat;
    }
    
    /**
     * Realitza una consulta a la Base de Dades insertant una nova fila
     * 
     */
    public void insertarVenda(){
        
        String[] Valors = {data, quantitat, producteID};
        
        DataBase.connect();
        DataBase.insert(Taula, columnesTaula, Valors);
        DataBase.disconnect();
        
        System.out.println("La venda s'ha introduit.");
    }
    
    /**
     * Realitza primer una validació de que la venda que volem eliminar existeix
     * en cas afirmatiu l'elimina
     */
    public void eliminaVenda(){
        
        String condicio = "producte_idproducte = " + producteID +
                          " AND datavenda = '" + data + "'" ;
    
        if(Validator.Validate(Taula, condicio)){
            
            DataBase.connect();
            DataBase.delete(Taula, condicio);
            DataBase.disconnect();
            
            System.out.println("La venda s'ha eliminat");
        }
        else{
            System.out.println("No hi ha cap venda que satisfaci les condicions");
        }
    }
    
    /**
     * Métode per establir la id màxima que es podrà introduir a la venda.
     * Ofereix orientació a l'hora de crear la venda.
     * 
     * @return maxID String
     */
    public static String[] identificadorMaxim(){
        
        String maxID[] = new String[2];
        
        DataBase.connect();
        ResultSet rs = DataBase.select("Producte", "MIN(idproducte) as minID, MAX(idproducte) as maxID", null, null, null, null);
        
        try {
            while(rs.next()){
                maxID[0] = rs.getString("minID");
                maxID[1] = rs.getString("maxID");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Venda.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return maxID;
    }
    
    
    
    /**
     * Formata l'impressió els atributs de l'objecte 
     * 
     * @return String
     */
    public String toString(){
        
        String unitats = "unitat";
        
        if(Integer.parseInt(quantitat) > 1)
            unitats += "s";
        
        return "- " + data.replace("-", "/") + ": " + quantitat + " " + unitats;
    }
}