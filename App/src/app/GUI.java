package app; 

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que crea l'interfície gràfica bàsica per a l'aplicació
 * Inclou el menú i cada una de les seves opcions
 */
public class GUI {
    
    /**
     * Variable que conté el títol de l'aplicació
     * 
     * @var String title
     */
    private String title;
    
    /**
     * Array que conté les diferents opcions del menú
     * 
     * @var String[] options
     */
    private String[] options;
    
    /**
     * Instanciació de la Base de Dades
     * 
     * @var DataBase DataBase
     */
    private final DataBase DataBase = new DataBase();
    
    /**
     * Constructor de la classe
     * Dibuixa les opcions del menú
     * 
     * @param String Title
     * @param String[] options
     */
    public GUI(String Title, String[] options){
        
        System.out.println(Title + ": \n**********************");
        
        for(int i=0; i< options.length; i++){
            System.out.println(i+1 + ". " + options[i]);
        }
    }
    
    /**
     * Métode que requereix les dades per introduir una nova venda 
     * a la Base de Dades. Valida les diferents dades
     * 
     * @return void
     */
    public void introduirVenda(){
        
        String dades[][] = Venda.Dades;
        String valorDades[] = new String[dades.length];
        
        String[] minMaxID = Venda.identificadorMaxim();
        
        dades[3][1] = minMaxID[0];
        dades[3][2] = minMaxID[1];
        
        for(int i = 0; i < dades.length; i++){
            
            if(i != 4){
                valorDades[i] = Validator.Validate(dades[i][0], dades[i][1], dades[i][2]);
            }
            else{
                valorDades[i] = Validator.Validate(dades[i][0], valorDades[3], "0");
            }
        }
        
        String dadesFinal[] = {valorDades[2] + "-" + valorDades[1] + "-" + valorDades[0],valorDades[3] , valorDades[4]};
        Venda venda = new Venda(dadesFinal);
        venda.insertarVenda();
    }
    
    /**
     * Métode que requereix les dades per eliminar una venda 
     * a la Base de Dades. Valida les diferents dades
     * 
     * @return void
     */
    public void eliminarVenda(){
        
        String dades[][] = Venda.Dades;
        String valorDades[] = new String[dades.length];
        
        String[] minMaxID = Venda.identificadorMaxim();
        
        dades[3][1] = minMaxID[0];
        dades[3][2] = minMaxID[1];
        
        for(int i = 0; i < dades.length-1; i++){
            
            valorDades[i] = Validator.Validate(dades[i][0], dades[i][1], dades[i][2]);
        }
        
        String dadesFinal[] = {valorDades[2] + "-" + valorDades[1] + "-" + valorDades[0],valorDades[3] , "null"};
        
        Venda venda = new Venda(dadesFinal);
        
        venda.eliminaVenda();
    }
    
    /**
     * Métode que requereix les dades per llistar les vendes realitzades
     * durant un mes de l'any. Valida les diferents dades
     * 
     * @return void
     */
    public void llistarVendesMes(){
        
        String dades[][] = Venda.Dades;
        String[] minMaxID = Venda.identificadorMaxim();

        dades[3][1] = minMaxID[0];
        dades[3][2] = minMaxID[1];
        
        String valorDades[] = new String[dades.length];
        
        for(int i = 1; i < dades.length-1; i++){
            
            valorDades[i] = Validator.Validate(dades[i][0], dades[i][1], dades[i][2]);
        }
        
        valorDades[0] = "";
        String mes = valorDades[1];
        String any = valorDades[2];
        String producteID = valorDades[3];
        
        String condicio = "producte_idproducte = " + producteID +
                          " AND datavenda BETWEEN '" + any + "-" + mes + "-1'"+
                          " AND '" + any + "-" + mes + "-31'" ;
    
        if(Validator.Validate("Venda", condicio)){
            
            DataBase.connect();
            ResultSet rs = DataBase.select("Venda", "*", null, condicio, null, null);
            
            try {
                while(rs.next()){
                
                    String[] resultatConsulta = new String[Venda.columnesTaula.length];
                    
                    for(int i = 0; i < Venda.columnesTaula.length; i++ ){
                        resultatConsulta[i] = rs.getString(Venda.columnesTaula[i]);
                    }
                    
                    Venda venda = new Venda(resultatConsulta);
                    
                    System.out.println(venda);
                }
            } catch (SQLException ex) {
                Logger.getLogger(Venda.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            DataBase.disconnect();
        }
        else{
            System.out.println("No hi ha vendes que satisfacin les condicions");
        }
    }
    
    /**
     * Métode que requereix les dades per mostrar els productes mes venuts 
     * durant un any. Valida les diferents dades
     * 
     * @return void
     */
    public void llistarProductesMes(){
        
        String any = Validator.Validate("Any", "1600", "3000");
        String condicio = "datavenda LIKE '"+any+"-%%-%%'";
    
        if(Validator.Validate("Venda", condicio)){
            
            DataBase.connect();
            ResultSet rs = DataBase.select("Venda", "producte_idproducte,sum(quantitat) as total", null, "datavenda LIKE '" + any + "-%%-%%' group by producte_idproducte", "total desc", null);

            try {
                
                int pos = 1;
                while (rs.next()) {
                    
                    String[] resultatConsulta = new String[Producte.columnesTaula.length];
                    
                    for(int i = 0; i < Producte.columnesTaula.length; i++ ){
                        resultatConsulta[i] = rs.getString(Producte.columnesTaula[i]);
                    }
                    
                    Producte producte = new Producte(resultatConsulta);
                    
                    System.out.println(pos + ")" + producte);
                    pos++;
                }
            } catch (SQLException ex) {
                Logger.getLogger(Venda.class.getName()).log(Level.SEVERE, null, ex);
            }

            DataBase.disconnect();
        
        }
        else{
            System.out.println("No hi ha vendes que satisfacin les condicions");
        }
    }
} 