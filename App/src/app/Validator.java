package app;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Clase per validar si una dada que s'introdueix per teclat es acceptada 
 */
public class Validator {
    
    private static final DataBase DataBase = new DataBase();
        
    /**
     * Mètode que requereix una entrada per teclat fins que sigui una dada vàlida
     * compresa entre un rang minim i máxim declarats per els atributs de l'objecte
     * 
     * @param text String Títol del missatge explicatiu
     * @param min valor mínim acceptat
     * @param max valor màxim acceptat
     * 
     * @return intDada int el valor introduit per teclat
     */
    public static String Validate(String text, String min, String max){
     
        boolean validated = false;
        int intDada = 0;
        String strDada = null;
        
        int minToInt = Integer.parseInt(min);
        int maxToInt = Integer.parseInt(max);
        
        do{
            
            if(text != "Quantitat"){
                
                System.out.print("Introdueix el " + text + " ["+ min + "-" + max + "]:");
            }
            else{
                System.out.print("Introdueix la " + text + ":");
            }
            
            Scanner dada = new Scanner(System.in);
            
            while (!dada.hasNextInt()) dada.next();
            
            intDada = dada.nextInt();
            
            if((intDada < minToInt || intDada > maxToInt) && text != "Quantitat"){
                System.out.println("La dada introduida és incorrecte");
            }
            else if(text == "Quantitat"){

                String idProducte = min;
                
                if(stockSuficient(intDada, idProducte)){
                    
                    validated = true;
                    
                    strDada = Integer.toString(intDada);

                    if (strDada.length() == 1) {
                        strDada = "0" + strDada;
                    }
                }
                else{
                    System.out.println("No hi ha suficient stockatge");
                }
                
                
            }
            else{
                validated = true;
                
                strDada = Integer.toString(intDada);
                
                if(strDada.length() ==1)
                    strDada = "0"+strDada;
            }
            
        }
        while(validated == false);
        
        return strDada;
    }
    
    /**
     * Mètode que requereix una entrada per teclat fins que sigui una dada
     * vàlida. La validesa dependedrà de si el registre existeix dins la Base de Dades
     * 
     * @param Taula
     * @param condicio
     * 
     * @return validat Boolean
     */
    public static boolean Validate(String Taula, String condicio){
        
        boolean validat = false;
        int intDada = 0;
        
        DataBase.connect();
        ResultSet rs =  DataBase.select(Taula, "*", null, condicio, null, null);

        int i = 0;

        try {
            while(rs.next()){
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Validator.class.getName()).log(Level.SEVERE, null, ex);
        }

        if(i >= 1){
            validat = true;
        }
        
        DataBase.disconnect();
        return validat;
    }
    
    private static boolean stockSuficient(int quantitat, String idProducte){
        
        boolean validat = false;
        int stock = 0;
        
        String strQuantitat = Integer.toString(quantitat);

        if (strQuantitat.length() == 1) {
            strQuantitat = "0" + strQuantitat;
        }
        
        DataBase.connect();
        ResultSet rs =  DataBase.select("Producte", "*", null, "idproducte =" + idProducte, null, null);
        
        try {
            while(rs.next()){
                
                String strStock = rs.getString("quantitat");
                stock = Integer.parseInt(strStock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Validator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DataBase.disconnect();
        
        if(stock >= quantitat)
            validat = true;
        
        return validat;
    }
}