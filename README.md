## Pràctica Final Base de Dades - Programació
Data d'entrega: 26 de maig de 2014

Autors: Aitor Delgado Vives, Xavier Tristancho Bordoy i Joanin.

#S'ha d'entregar:

- Informe documentant tot el treball.

	#L'informe ha de constar de:

	- Introducció.
	- Explicació de l'entorn de treball.
	- Disseny ER de la Base de Dades.
	- Model Relacional.
	- Diccionari de Dades.
	- Diagrama de Classes.
	- Diccionari de Classes.
	- Com executar el programa.
	- Conclusions.

- El projecte de NetBeans.
- Els Scripts de les creació de les taules.
- Els Scripts dels inserts a les taules.
 
 
 
 
